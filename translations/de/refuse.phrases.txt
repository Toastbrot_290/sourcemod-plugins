﻿"Phrases"
{
	"prefix"
	{
		"de"		"{fullred}[Verweigern] "
	}
	"error_all_refused"
	{
		"de"		"{lavender}Es haben bereits alle Terroristen verweigert!"
	}
	"error_already_refusing"
	{
		"de"		"{lavender}Es hat bereits jemand nach Verweigerungen gefragt!"
	}
	"error_dead"
	{
		"de"		"{lavender}Es hat bereits jemand nach Verweigerungen gefragt!"
	}
	"error_team_wrong"
	{
		"de"		"{lavender}Du bist im falschen Team!"
	}
	"RefuseQuestionMenuTitle"
	{
		"de"		"Möchtest du Verweigern?"
	}
	"Refuse"
	{
		"de"		"Natürlich! Ich bin feige!"
	}
	"NoRefuse"
	{
		"de"		"Nein! Ich bin mutig!"
	}
	"RefuseAwnserMenuTitle"
	{
		"de"		"Spieler die verweigert haben:"
	}
	"StopRefusing"
	{
		"de"		"{lavender}Verweigern beendet!"
	}
	"StartRefusing"
	{
		"de"		"{lavender}Terroristen können nun für {1} Sekunden verweigern!"
	}
	"player_refused"
	{
		"de"		"{blue}{1} {lavender}hat das Spiel verweigert!"
	}
}
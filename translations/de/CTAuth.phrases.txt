﻿"Phrases"
{
	"prefix"
	{
		"de"		"{fullred}[CTAuth]"
	}
	"prefix_plain"
	{
		"de"		"[CTAuth]"
	}
	"error_not_enough_points"
	{
		"de"		"{lavender}Du kannst dem CT Team nicht beitreten! Du brauchst noch {blue}{3}{lavender} Stamm Punkte!"
	}
	"error_not_enough_points_plain"
	{
		"de"		"Du kannst dem CT Team nicht beitreten! Du brauchst noch {3} Stamm Punkte!"
	}
	"error_not_enough_points_all"
	{
		"de"		"{lavender}{blue}{1} {lavender} hat zu wenig Stamm Punkte um dem CT Team beizutreten!"
	}
}
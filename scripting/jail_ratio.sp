#include <sourcemod>
#include <sdktools>
#include <multicolors>
#include <autoexecconfig>

#undef REQUIRE_PLUGIN
#include <updater>

#define UPDATE_URL		"http://bitbucket.toastdev.de/sourcemod-plugins/raw/master/JailRatio.txt"

public Plugin:myinfo = 
{
	name = "Jail Ratio",
	author = "Toast",
	description = "A simple Jail ratio",
	version = "0.0.1",
	url = "bitbucket.toastdev.de"
}

/************************
	Global Variabels
*************************/

// ConVar Handle
new Handle:g_cRatio = INVALID_HANDLE;
new Handle:g_cBlockRounds = INVALID_HANDLE;
new Handle:g_cRestrictRatio = INVALID_HANDLE;

// Integer
new g_iBlockRounds = 0;

// Float
new Float:g_fRatio = 0.5;

// Boolean
new bool:g_bRestrictRatio = false;

// Array
new g_aPlayerBlockRounds[MAXPLAYERS + 1];

public OnPluginStart()
{
	LoadTranslations("jratio.phrases");

	// Handle the CVars
	g_cRatio = AutoExecConfig_CreateConVar("sm_jratio_ratio", "0.5", "How many CTs should be for one T?");
	g_cBlockRounds = AutoExecConfig_CreateConVar("sm_jratio_block_rounds", "0", "How many Rounds should somone get blocked from CT Team, if the ratio is bad, if he gets swaped by the plugin?");
	g_cRestrictRatio = AutoExecConfig_CreateConVar("sm_jratio_restrict_ratio", "0", "Shall the ratio fit always? (Lock CT Team if it's full) (1 = Enable / 0 = Disable)", _, true, 0.0, true, 1.0);

	HookConVarChange(g_cRatio, ConVarChanged);
	HookConVarChange(g_cBlockRounds, ConVarChanged);
	HookConVarChange(g_cRestrictRatio, ConVarChanged);

	AutoExecConfig_ExecuteFile();
	AutoExecConfig_CleanFile();

	HookEvent("player_death", Event_PlayerDeath_Callback);
	HookEvent("player_connect", Event_PlayerConnect_Callback);
	HookEvent("player_disconnect", Event_PlayerDisconnect_Callback);
	HookEvent("round_start", Event_RoundStart_Callback);

	AddCommandListener(TeamJoin, "jointeam");
}
public OnLibraryAdded(const String:name[])
{
    if (StrEqual(name, "updater"))
    {
        Updater_AddPlugin(UPDATE_URL)
    }
}

public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
   MarkNativeAsOptional("Updater_AddPlugin");
   return APLRes_Success;
}

public ConVarChanged(Handle:convar, const String:oldValue[], const String:newValue[]) 
{
	if(convar == g_cRatio)
	{
		g_fRatio = StringToFloat(newValue);
	}
	else if(convar == g_cBlockRounds)
	{
		g_iBlockRounds = StringToInt(newValue);
	}
	else if(convar == g_cRatio)
	{
		g_bRestrictRatio = GetConVarBool(convar);
	}
}

public Action:TeamJoin(client, const String:command[], args)
{
	// Somone tries to join a team by himself
	if(!IsClientInGame(client)){
		return Plugin_Stop;
	}

	new String:Team[2];
	GetCmdArg(1, Team, sizeof(Team));
	new TargetTeam = StringToInt(Team);

	if(TargetTeam == 3 || (TargetTeam == 0 || TargetTeam > 3))
	{
		if(!CheckRatio(client))
		{
			if(g_bRestrictRatio)
			{
				CPrintToChat(client, "%t %t", "prefix", "error_ratio_not_good");
				return Plugin_Stop;
			}
			else if(g_aPlayerBlockRounds[client] >= 1)
			{
				CPrintToChat(client, "%t %t", "prefix", "error_ratio_not_good");
				return Plugin_Stop;
			}
			else
			{
				CPrintToChatAll("%t %t", "prefix", "warning_ratio_not_good");
				return Plugin_Continue;
			}
		}
		return Plugin_Continue;
	}
	return Plugin_Continue;

}

public Event_PlayerDeath_Callback(Handle:event, const String:name[], bool:dontBroadcast)
{
	new p_iUserid = GetEventInt(event, "userid");
	new p_iClient = GetClientOfUserId(p_iUserid);

	if(!CheckRatio())
	{
		CPrintToChat(p_iClient, "%t %t", "prefix", "error_ratio_not_good");
		ChangeClientTeam(p_iClient, 2);
		g_aPlayerBlockRounds[p_iClient] = g_iBlockRounds;
	}
}

public Event_PlayerDisconnect_Callback(Handle:event, const String:name[], bool:dontBroadcast)
{
	// Someone left the game
	new p_iUserid = GetEventInt(event, "userid");
	new p_iClient = GetClientOfUserId(p_iUserid);

	g_aPlayerBlockRounds[p_iClient] = 0;
}

public Event_PlayerConnect_Callback(Handle:event, const String:name[], bool:dontBroadcast)
{
	// Someone joined the game
	new p_iUserid = GetEventInt(event, "userid");
	new p_iClient = GetClientOfUserId(p_iUserid);

	g_aPlayerBlockRounds[p_iClient] = 0;
}

public Event_RoundStart_Callback(Handle:event, const String:name[], bool:dontBroadcast)
{
	for(new i = 0; i <= MAXPLAYERS; i++)
	{
		if(IsClientInGame(i) && g_aPlayerBlockRounds[i] != 0)
		{
			g_aPlayerBlockRounds[i] = g_aPlayerBlockRounds[i] - 1;
		}
	}
}

CheckRatio(any:p_iClient = 0)
{
	new Float:p_fCTCount = float(GetTeamClientCount(3));
	new Float:p_fTCount = float(GetTeamClientCount(2));

	if(p_iClient != 0)
	{
		if(GetClientTeam(p_iClient) == 2)
		{
			p_fCTCount++;
			p_fTCount--;
		}
		else if(GetClientTeam(p_iClient) != 3)
		{
			p_fCTCount++;
		}
	}

	if((p_fCTCount / p_fTCount) > g_fRatio)
	{
		return false;
	}

	return true;
}
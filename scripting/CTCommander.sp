#include <sourcemod>
#include <morecolors>
#include <sdktools>
#include <voiceannounce_ex>
#include <basecomm>
#undef REQUIRE_PLUGIN
#include <updater>

#define UPDATE_URL    "http://bitbucket.toastdev.de/sourcemod-plugins/raw/master/CTCommander.txt"
public Plugin:myinfo = 
{
	name = "CTCommander",
	author = "Toast",
	description = "A new CT Commadner Plugin",
	version = "0.0.1",
	url = "toastdev.de"
}
new Commander = -1;
new Auto = 0;
new Broadcast = 0;
new Handle:c_Auto;
new Handle:c_Broadcast;
new Handle:c_Voice;
new Voice = 0;
new ColorCommander = 1;

public OnPluginStart()
{
	LoadTranslations("CTCommander.phrases");
	
	HookEvent("player_disconnect", PlayerDissconnect);
	HookEvent("player_death", PlayerDeath);
	HookEvent("round_start", RoundStart);
	
	c_Auto = CreateConVar("commander_auto", "0", "1 = Automattically choose Warden");
	c_Broadcast = CreateConVar("commander_broadcast", "0", "1 = Use Hint and Center Text for Notifications");
	c_Voice = CreateConVar("commander_voice", "0", "1 = Enable Voice Override");
	
	AutoExecConfig();
	
	Auto = GetConVarInt(c_Auto);
	Broadcast = GetConVarInt(c_Broadcast);
	Voice = GetConVarInt(c_Voice);
	
	HookConVarChange(c_Auto, ConVarChanged);
	HookConVarChange(c_Voice, ConVarChanged);
	HookConVarChange(c_Broadcast, ConVarChanged);
	
	RegConsoleCmd("sm_ctl", CommandGetCommander);
	RegConsoleCmd("sm_ectl", CommandExitCommander);
	RegConsoleCmd("sm_rand", CommandRand);
	
	RegConsoleCmd("sm_voice", CommandToggleVoiceOverride);
	RegConsoleCmd("sm_broadcast", CommandToggleBroadcast);
	
	if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
	
}

public OnLibraryAdded(const String:name[])
{
    if (StrEqual(name, "updater"))
    {
        Updater_AddPlugin(UPDATE_URL)
    }
}
public bool:OnClientSpeakingEx(client)
{
	if(!BaseComm_IsClientMuted(client))
	{
		if(Commander != -1){
			if(Commander != client){
				if(IsClientSpeaking(Commander) && Voice == 1){
					BaseComm_SetClientMute(client, true);
					CreateTimer(2.0, demute, client);
					PrintCenterText(client, "%t %t", "prefix_plain", "commander_silent");
					return false;
				}
			}
			else{
				return true;
			}
		}
		else{
			return true;
		}
	}
	else{
		return false;
	}
	return true;
}
public Action:demute(Handle:timer, any:client)
{
	if (IsClientInGame(client) && !IsFakeClient(client) &&	BaseComm_IsClientMuted(client)){
		BaseComm_SetClientMute(client, false);
	}
}
public ConVarChanged(Handle:cvar, const String:oldValue[], const String:newValue[]) {
	
	if(cvar == c_Auto){
		Auto = StringToInt(newValue);
	}
	else if(cvar == c_Voice){
		Voice = StringToInt(newValue);
	}
	else if(cvar == c_Broadcast){
		Broadcast = StringToInt(newValue);
	}
	
}
public Action:RoundStart(Handle:event, const String:name[], bool:dontBroadcast) 
{
	Commander = -1;
	Voice = GetConVarInt(c_Voice);
	Broadcast = GetConVarInt(c_Broadcast);
	if(Auto == 1){
		ChooseCommander();
	}
	
}
public Action:PlayerDissconnect(Handle:event, const String:name[], bool:dontBroadcast) 
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if(Commander == client){
		Commander = -1;
		Voice = GetConVarInt(c_Voice);
		Broadcast = GetConVarInt(c_Broadcast);
		if(Auto){
			ChooseCommander();
			SetEntityRenderColor(client, 255, 255, 255, 255);
		}
		else{
			CPrintToChatAll("%t %t", "prefix", "commander_disconnected", client);
			if(Broadcast == 1){
				PrintCenterTextAll("%t %t", "prefix_plain", "commander_disconnected_plain", client);
				PrintHintTextToAll("%t %t", "prefix_plain", "commander_disconnected_plain", client);
			}
		}
	}
}
public Action:PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) 
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if(Commander == client){
		Commander = -1;
		Voice = GetConVarInt(c_Voice);
		Broadcast = GetConVarInt(c_Broadcast);
		if(Auto){
			ChooseCommander();
			SetEntityRenderColor(client, 255, 255, 255, 255);
		}
		else{
			CPrintToChatAll("%t %t", "prefix", "commander_dead", client);
			if(Broadcast == 1){
				PrintCenterTextAll("%t %t", "prefix_plain", "commander_dead_plain", client);
				PrintHintTextToAll("%t %t", "prefix_plain", "commander_dead_plain", client);
			}
		}
	}
}
public Action:CommandRand(client, args)
{
	for(new i=1; i <= MAXPLAYERS; i ++){
		new randomint = GetRandomInt(1, 10);
		PrintToChatAll("Random Value: %i", randomint);
	}
	return Plugin_Continue;
}
public Action:CommandGetCommander(client, args)
{
	if(Commander == -1)
	{
		if(GetClientTeam(client) == 3)
		{
			
			if(IsPlayerAlive(client))
			{
				SetTheCommander(client);
			}
			else{
				CPrintToChat(client, "%t %t", "prefix", "commander_playerdead");
			}
		}
		else{
			CPrintToChat(client, "%t %t", "prefix", "commander_ctsonly");
		}
	}
	else{
		CPrintToChat(client, "%t %t", "prefix", "commander_exist", Commander);
	}
	return Plugin_Continue;
}
public Action:CommandExitCommander(client, args)
{
	if(Commander == client){
		Voice = GetConVarInt(c_Voice);
		Broadcast = GetConVarInt(c_Broadcast);
		if(Auto){
			ChooseCommander();
			SetEntityRenderColor(client, 255, 255, 255, 255);
		}
		else{
			Commander = -1;
			CPrintToChatAll("%t %t", "prefix", "commander_retire", client);
			if(Broadcast == 1){
				PrintCenterTextAll("%t %t", "prefix_plain", "commander_retire_plain", client);
				PrintHintTextToAll("%t %t", "prefix_plain", "commander_retire_plain", client);
			}
		}
	}
	else{
		CPrintToChat(client, "%t %t", "prefix", "commander_notcommander");
	}
}
public Action:CommandToggleVoiceOverride(client, args)
{
	if(client == Commander){
		if(Voice == 1)
		{
			Voice = 0;
			CPrintToChat(client, "%t %t", "prefix", "commander_voice_deactivated");
		}
		else{
			Voice = 1;
			CPrintToChat(client, "%t %t", "prefix", "commander_voice_activated");
		}
	}
	else{
		CPrintToChat(client,"%t %t", "prefix", "commander_no_perms_no_commander");
	}
	
}
public Action:CommandToggleBroadcast(client, args)
{
	if(client == Commander){
		if(Broadcast == 1)
		{
			Broadcast = 0;
			CPrintToChat(client, "%t %t", "prefix", "commander_broadcast_deactivated");
		}
		else{
			Broadcast = 1;
			CPrintToChat(client, "%t %t", "prefix", "commander_broadcast_activated");
		}
	}
	else{
		CPrintToChat(client,"%t %t", "prefix", "commander_no_perms_no_commander");
	}
	
}
public SetTheCommander(client)
{
	Commander = client;
	CPrintToChatAll("%t %t", "prefix", "commander_new", client);
	if(Broadcast == 1){
		PrintCenterTextAll("%t %t", "prefix_plain", "commander_new_plain", client);
		PrintHintTextToAll("%t %t", "prefix_plain", "commander_new_plain", client);
	}
	if(ColorCommander == 1){
		SetEntityRenderColor(client, 0, 255, 0, 255);
	}
}
public ChooseCommander()
{
	new cts = GetTeamClientCount(3);
	if(cts <= 0){
		return;
	}
	new indexs = 0;	
	new CTArray[MaxClients];
	for (new i = 1; i <= MaxClients; i++)
	{	
		if(IsClientInGame(i)){
			if(IsPlayerAlive(i) && GetClientTeam(i) == 3  && Commander != i){
				CTArray[indexs] = i;
				indexs = indexs + 1;
			}
		}
	}
	if(indexs != 0)
	{
		new NewCommander = GetRandomInt(0, indexs - 1);
		SetTheCommander(CTArray[NewCommander]);
	}
	return;
}
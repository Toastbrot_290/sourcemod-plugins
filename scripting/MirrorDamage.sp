#include <sourcemod>
#include <morecolors>
#include <sdkhooks>
#include <sdktools>

#undef REQUIRE_PLUGIN
#include <updater>

#define UPDATE_URL    "http://bitbucket.toastdev.de/sourcemod-plugins/raw/master/MirrorDamage.txt"

public Plugin:myinfo = 
{
	name = "MirrorDamgae",
	author = "Toast",
	description = "A Mirror Damage plugin",
	version = "1.0",
	url = "toastdev.de"
}
new Mirror[MAXPLAYERS +1];
new lateLoad;

public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
	if (late == true)
	{
		lateLoad = true;
	}
}
public OnClientPutInServer(client)
{
	//Knife Count und Knife Fails auf 0 setzen
	Mirror[client] = 0;

	SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
}
public OnPluginStart()
{
	
	
	RegAdminCmd("sm_mirror", MirrorCommandHandler, FlagToBit(Admin_Kick))
	
	LoadTranslations("mirror.phrases");
	for (new i = 1; i <= MaxClients; i++)
	{
		Mirror[i] = 0;
	}
	
	if (lateLoad)
	{
		for (new i = 1; i <= MaxClients; i++)
		{
			if (IsClientInGame(i))
			{
			//Hook aktivieren
			SDKHook(i, SDKHook_OnTakeDamage, OnTakeDamage);
			}
		}
	}
	
	if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }

	
}
public OnLibraryAdded(const String:name[])
{
    if (StrEqual(name, "updater"))
    {
        Updater_AddPlugin(UPDATE_URL)
    }
}
public Action:OnTakeDamage(client, &attacker, &inflictor, &Float:damage, &damagetype)
{
	if(Mirror[attacker] == 1)
	{
		new health = GetClientHealth(attacker);
		new newhealth = health - 2*RoundFloat(damage);
		if(newhealth > 0)
		{
			SetEntityHealth(attacker, health - RoundFloat(damage));
		}
		else{
			ForcePlayerSuicide(attacker);
		}
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action:MirrorCommandHandler(client, args)
{
	new String:string[64];
	new Handle:menu = CreateMenu(MirrorMenuHandler);
	Format(string,sizeof(string),"%t", "MirrorMenuTitle", LANG_SERVER);
	SetMenuTitle(menu, string);
	for (new i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i)){
			new String:info[32];
			IntToString(i, info, sizeof(info));
			GetClientName(i, string, sizeof(string));
			if(Mirror[i] == 1)
			{
				StrCat(string, sizeof(string), "[X]");
			}
			AddMenuItem(menu, info, string);
		}
	}
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}


public MirrorMenuHandler(Handle:menu, MenuAction:action, client, param2)
{
	if (action == MenuAction_Select)
	{
		new String:info[64];
		new p;
		new String:clientname[132];
		GetMenuItem(menu, param2, info, sizeof(info));
		p = StringToInt(info);
		ToggleMirror(p);
		GetClientName(p,clientname,sizeof(clientname));
		CPrintToChat(client,"%t %t", "prefix", "toggled_mirror", clientname)
	}
}


ToggleMirror(client)
{
	if(IsClientInGame(client))
	{
		if(Mirror[client] == 0){
			Mirror[client] = 1;
		}
		else{
			Mirror[client] = 0;
		}
	}
	
}
